#!/usr/bin/python3

import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import json


"""
Script to add taskline entries to google calendar.  Script assumes that 
credentials.json, and event_default.json are in the same folder as this script.

Requires google oauth accsess to work.
"""



tl_data = "~/snap/taskline/current/.taskline/storage/storage.json"

# authenticate this script with google's api.
def auth():
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return creds

# Checks taskline's json file for differences between the last time we ran and now
def update_tasks():
    with open(tl_data, "r") as f:
        data = json.load(f)
    try:
        with open("tasks.pickle", "rb") as f:
            tasks = pickle.load(f)
    except:
        tasks = []
    
    deleted = []
    new = []

    # Check for new entries
    for t in data:
        if t["isTask"] and t not in tasks and t["dueDate"] != 0:
            new.append(t)
            tasks.append(t)


    # Check for deleted entries
    for t in tasks:
        if t not in data:
            deleted.append(t["id"])
            tasks.remove(t)

    # Write updated list to pickle.
    with open("tasks.pickle", "wb") as f:
        pickle.dump(tasks, f)

    return {"new": new, "deleted": deleted, "tasks": tasks}


# Create a calendar event on google cal.
def create_event(task):
    # Load default event structure.
    with open("event_default.json", "r") as f:
        event = json.load(f)

    # Populate event.
    star_char = ""
    if task["isStarred"]:
        star_char = "⭐"

    check_char = "☐"
    if task["isComplete"]:
        check_char = "🗹"

    event["summary"] = "TL_{id}: {star} {desc} {check}".format(
            id=str(task["id"]),
            star=star_char,
            desc=task["description"],
            check=check_char)

    event_start = datetime.datetime.utcfromtimestamp(
            task["dueDate"] // 1000).strftime('%Y-%m-%d')

    event_end = event_start

    event["start"]["date"] = event_start
    event["end"]["date"] = event_end

    event["reminders"]["overrides"] = []
    event["reminders"]["useDefault"] = True

    return event


# Get the taskline id from a calendar entry's summary string.
def get_id(summary):
    res = ""
    for i in range(3, len(summary)):
        if summary[i] in "1234567890":
            res = res + summary[i]
        else:
            break
    
    if res == "":
        return None

    return int(res)


# Delete event from google canendar
def delete_event(event, service):
    eid = event["id"]
    service.events().delete(calendarId='primary', eventId=eid).execute()


# Handles updating the calendar.
def update_cal(task_dict, creds):
    # Google cal api.
    service = build('calendar', 'v3', credentials=creds)


    # Get current events from google cal. (Max 500)
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=500, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])

    if not events:
        return

    # Delete events if needed.
    for event in events:
        if event["summary"][0:3] == "TL_":
            id = get_id(event["summary"])
            if id in task_dict["deleted"]:
                print("Deleting event id:", str(id)) 
                delete_event(event, service)
    

    # Add new events.
    for e in task_dict["new"]:
        print("Adding event id:", e["id"])
        new_event = create_event(e)
        #print(new_event)
        event = service.events().insert(calendarId='primary', body=new_event, sendNotifications=True).execute()


def main():
    print("Syncing tl with Calendar...")
    tsk_dict = update_tasks()
    if tsk_dict["deleted"] == [] and tsk_dict["new"] == []:
        print("No relevant changes.")
        return
    creds = auth()
    update_cal(tsk_dict, creds)
    print("Done")



if __name__ == '__main__':
    main()
