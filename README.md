# tl-google-cal
---
Taskline integration for google calendar

Taskline is a command line task managment app, and can be found [here](https://github.com/perryrh0dan/taskline)

# Instalation
* This script must first be authenticated with [OAuth](https://developers.google.com/identity/protocols/OAuth2), then you may copy the credentials.json file to the working directory. (This is kinda painful, but an example credentials.json file is included in this repo.)
* Next move tl to a folder in your `$PATH`. (Ensure there is no conflicts with the name `tl`)
* now copy all remaining files to a new directory at `/opt/tl-sync/`

Taskline can now be run from anywhere by calling `tl` with the regular args.